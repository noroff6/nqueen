#include <iostream>
#include <vector>

#ifndef QUEEN_H
#define QUEEN_H
bool canPlaceQueen(int col, int row, std::vector<std::vector<char>> &board, int N);
void placeQueen(int row, int N, std::vector<std::vector<char>> &board);
void printNumbers(int N);
#endif