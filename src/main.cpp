#include <iostream>
#include <vector>
#include <chrono>
#include "queen.h"

int main()
{
    int totalQueens;
 
    std::cout << "Enter n queens between 1 and 10: " << std::endl;
    std::cin >> totalQueens;
    while (totalQueens < 1 || totalQueens > 10)
    {
        std::cout << "Please enter a number between 1 and 10: " << std::endl;
        std::cin >> totalQueens;
        break;
    }
    std::cout << std::endl;

    // Create empty 2D board
    std::vector<std::vector<char>> board(totalQueens, std::vector<char>(totalQueens, '*'));

    // Measure time for function
    auto t1 = std::chrono::high_resolution_clock::now();
    placeQueen(0, totalQueens, board); //Call function with column 0
    auto t2 = std::chrono::high_resolution_clock::now();

    // Duration of execution in microseconds
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

    printNumbers(totalQueens);
    std::cout << "Execution time = " << duration << " microseconds" << std::endl;
    return 0;
}
