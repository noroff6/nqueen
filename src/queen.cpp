#include "queen.h"

int total_solutions = 0;
int total_attempts = 0;

bool canPlaceQueen(int row, int col, std::vector<std::vector<char>> &board, int N)
{
    // Check row and diagonals for clashes with other queens
    for(int i = 0; i <= col; i++) {
		    if(board[row][i] == 'Q') 
            return false; 
		    if(row - i >= 0 && col - i >= 0 && board[row - i][col - i] == 'Q') 
            return false;
		    if(col - i >= 0 && row + i <  N && board[row + i][col - i] == 'Q') 
            return false;
	    }

    return true;
}

void placeQueen(int column, int totalQueens, std::vector<std::vector<char>> &board)
{   
    // Start from first row
    int row = 0;

    // If all queens have been placed
    if (column == totalQueens)
    {
        // Add to total solutions
        total_solutions++;

        std::cout << "Solution " << total_solutions << std::endl;
        // Display solution boards
        for (size_t i = 0; i < board.size(); i++)
        {
            for (size_t j = 0; j < board[i].size(); j++)
                std::cout << "|" << board[i][j] << "|";
            std::cout << std::endl;
        }
        std::cout << std::endl;
    }

    // Continue backtracking 
    else
    {
        // Start from first column
        while (row >= 0 && row < totalQueens)
        {
            // Total attempts
            total_attempts++;

            // Check if queen can be placed
            if (canPlaceQueen(row, column, board, totalQueens))
            {
                // Place queen
                board[row][column] = 'Q';
                // Go to next column and recurse
                placeQueen(column + 1, totalQueens, board);
            }
            // Remove queen if it cannot be placed
            board[row][column] = '*';
            row++;
        }
    }
}

// Print number of solutions and attempts
void printNumbers(int N)
{
    std::cout << total_solutions << " solutions"
              << " for " << N << " queens" << std::endl;
    std::cout << "Total attempts = " << total_attempts << std::endl;
}