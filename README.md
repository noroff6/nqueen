# N-Queens Problem / C++

## Description
This project is an assignment from [Noroff](https://www.noroff.no/en/), as part of [Experis Academy](https://www.experis.se/sv/it-tjanster/experis-academy). It contains a solution for the N-Queens problem coded in C++.
<br />

## Table of Contents
- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Contributing](#contributing)
- [References](#references)
- [License](#license)

## Background
N number of queens will be placed on a NxN chessboard and cannot clash with each other: They cannot share column, row or diagonal.
<br />
<br />
**Backtracking** <br />
We start by placing one queen in a column, and then me move on to the next column. A method called ```canPlaceQueen()``` checks if the position of the queen is safe or not by checking rows and diagonals. If no clashes with other queens occur we place the queen on the board and then call the function ```placeQueen()``` (recursion) again and move to the next column. <br />
If no queen can be placed on the board we remove the queen and backtrack, which means it moves to the previous step.
<br />
<br />
**Assignment:** <br />
Write a C++ program that receives an integer, called n, in the range [1, 10], and prints out at least one solution (if any exist) to the problem of n-queen. (In this problem, n queens should be put on an n*n chess board such that no queen can attack any other.)

## Install
Clone the repo.

Install CMake: 
```
$ sudo apt install cmake
```

## Usage
```
./main
```
1. Enter number of queens (N) between 1 and 10
2. A NxN grid will be created with N number of queens
3. The code uses backtracking as a method, and all possible solutions for N queens between 1 and 10 will be displayed in the terminal
4. Number of attempts and execution time in microseconds for the function will also be printed
<br />
<br />
*Example output:*<br />
![Example](images/nqueens6.PNG)

## Contributing
This project does not use any contributors, but feel free to use the code and optimize it in your own unique way.

## References
-  [N Queen Problem | Backtracking-3](https://www.geeksforgeeks.org/n-queen-problem-backtracking-3/)
-  [N-Queens problem using backtracking in Java/C++](https://www.digitalocean.com/community/tutorials/n-queens-problem-java-c-plus-plus)
-  [The N-Queens Problem Using C++ (YouTube)](https://youtu.be/zCPkFQwXihc)

## License
[MIT](docs/LICENSE.md)